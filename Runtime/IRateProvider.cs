using System;

namespace UMNP.GamedevKit.RateUs
{
    public interface IRateProvider
    {
        void Refresh();
        void StartReview(Action onFinished);
    }
}
