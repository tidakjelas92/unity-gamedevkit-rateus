using System;
using UMNP.GamedevKit.Configs;
using UMNP.GamedevKit.Utils;
using UnityEngine;

namespace UMNP.GamedevKit.RateUs
{
    public class MockRateUs : MonoBehaviour, IRateProvider
    {
        [Header("External References")]
        [SerializeField] private App _appConfig;
        [SerializeField] private MockRateUsPanel _panel;

        private MockRateUsPanel _panelInstance;
        private Canvas _canvas;
        private bool _isInitialized;

        private Action _pause;
        private Action _resume;

        private void Awake()
        {
            DontDestroyOnLoad(this);
            _canvas = GetComponent<Canvas>();
        }

        private void Start() => _canvas.enabled = false;

        public void Initialize(Action pause, Action resume)
        {
            if (_isInitialized) return;

            _pause = pause;
            _resume = resume;

            _isInitialized = true;
        }

        [ContextMenu("Refresh")]
        public void Refresh()
        {
            if (!_panelInstance)
                _panelInstance = Instantiate(_panel, transform);

            _panelInstance.Refresh(_appConfig ? _appConfig.Id : "Game title.");
            _panelInstance.gameObject.SetActive(false);
        }

#if UNITY_EDITOR
        [ContextMenu("StartReview")]
        private void StartReviewContextMenu() => StartReview(() => Logging.LogTrace("Rate is finished.", this));
#endif

        public void StartReview(Action onFinished)
        {
            if (!_panelInstance) return;

            _canvas.enabled = true;
            _panelInstance.SetOnFinished(
                () =>
                {
                    _canvas.enabled = false;
                    _resume?.Invoke();
                    onFinished?.Invoke();
                }
            );
            _panelInstance.gameObject.SetActive(true);
            _pause?.Invoke();
        }
    }
}
