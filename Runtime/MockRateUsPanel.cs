using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UMNP.GamedevKit.RateUs
{
    public class MockRateUsPanel : MonoBehaviour
    {
        [Header("Internal References")]
        [SerializeField] private TextMeshProUGUI _titleText;
        [SerializeField] private TMP_InputField _messageInputField;
        [SerializeField] private Image[] _stars;
        [Header("Parameters")]
        [SerializeField] private Color _emptyStarColor;
        [SerializeField] private Color _filledStarColor;

        private Action _onFinished;

        public void Refresh(in string gameTitle)
        {
            _titleText.SetText(gameTitle);
            _messageInputField.SetTextWithoutNotify(string.Empty);
            SetStar(_stars.Length);
        }

        public void SetOnFinished(Action onFinished) => _onFinished = onFinished;

        public void SetStar(int count)
        {
            for (var i = 0; i < count; i++)
                _stars[i].color = _filledStarColor;

            for (var i = _stars.Length - 1; i > count - 1; i--)
                _stars[i].color = _emptyStarColor;
        }

        public void OnCloseButtonClicked()
        {
            Destroy(gameObject);
            _onFinished?.Invoke();
        }
    }
}
