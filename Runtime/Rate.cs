using System;

namespace UMNP.GamedevKit.RateUs
{
    public static class Rate
    {
        private static IRateProvider _provider;

        public static void Initialize(IRateProvider provider) => _provider = provider;
        public static void Refresh() => _provider?.Refresh();
        public static void StartReview(Action onFinished) => _provider?.StartReview(onFinished);
    }
}
